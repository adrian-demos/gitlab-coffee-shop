Hello,

This is a follow-up note relating to your reported ticket:

ID: %{ISSUE_ID}  
PATH: %{ISSUE_PATH}  

Details:  
%{NOTE_TEXT}

Regards  
The Coffee Shop Support Team
