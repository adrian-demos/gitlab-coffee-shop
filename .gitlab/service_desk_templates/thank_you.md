Thank you for submitting your issue relating to our Coffee Shop application.

Your issue has been assigned a unique identifier, %{ISSUE_ID}, which you should use in any subsequent communication relating to this matter.

The Issue Path for this issue is %{ISSUE_PATH}.

We will be back in touch as soon as possible.

Regards  
The Coffee Shop Support Team
